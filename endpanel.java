import javax.swing.*;                                                           //Imports
import java.awt.*;
import javax.imageio.ImageIO;
import java.awt.image.*;
import java.awt.event.*;

/**
 * Displays the endpanel after the game.
 */
public class endpanel extends JPanel {                                          //Class definition
  /**
   * Checks as to whether or not play has been pressed.
   */
  private static boolean playPressed = false;                                   //Stores whether or not to continue

  /**
   * Draws everything on the JFrame in resources to display the score.
   * @param The JFrame graphics system
   */
  public void paintComponent( Graphics g ) {                                    //Is called to draw the paneling
    JButton exit = new JButton( "" );                                           //Creates a button
    exit.setBounds( 1000, 550, 250, 150 );                                      //Sets position and size of button
    exit.setVisible( true );                                                    //Allows user interaction with button
    exit.setOpaque( false );                                                    //Makes button invisible
    exit.setContentAreaFilled( false );                                         //Makes any parts also invisible
    exit.setBorderPainted( false );                                             //Removes button
    exit.addActionListener(                                                     //Adds listener
      new AbstractAction( "exit" ) {                                            //Initializes listener to anon. method
        public void actionPerformed( ActionEvent e ) {                          //Defines said anon. method
          System.exit( 0 );                                                     //Exits on button press
        }
      }
    );
    resources.frame.add( exit );                                                //Adds button to jframe


    g.drawImage( (BufferedImage)resources.textures.obtain( "endpanel" ),        //Draws background
      0, 0, null );
    g.setFont( new Font( "Serif", Font.BOLD, 145 ) );                           //Set font
    g.setColor( new Color( 0, 0, 0 ) );                                         //Set text color
    g.drawString( Integer.toString( resources.getScore() ), 525, 666);          //Draw score
  }
}
