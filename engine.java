import java.lang.*;   //Obtains nanotime                                        //Imports
import javax.swing.*; //Graphics
import java.awt.*;

/**
 * Carries out the functions of the game
 */
public class engine {                                                           //Class header
  /**
   * Starts the game
   */
  public engine() {                                                             //Constructor, runs game
    //Variables
    final int MAX_ENTITIES = 500;                                               //Defines the amount of non-projectile enemies allowed
    final int MAX_PROJECTILES = 50;                                             //Defines the amount of projectiles allowed
    final int MAX_TEXTURES = 80 * 45 + 5 + MAX_ENTITIES + MAX_PROJECTILES;      //Defines the size of the graphics buffer

    //Generic setups
    display disp = new display();                                               //Creates graphics mediator
    DisplayObject[] texts = new DisplayObject[ MAX_TEXTURES ];                  //Instantiates buffer
    //Special setups
    resources.entities = new entity[ MAX_ENTITIES ];                            //Instantiates buffer
    resources.projectiles = new projectile[ MAX_PROJECTILES ];                  //Instantiates buffer
    resources.entities[ 0 ] = new player();                                     //Creates player as entity 0
    double deltaTime = .05; //Acts as a physics multiplier                      //Defines initial deltatime
    long startTime = System.nanoTime();                                         //Grabs system time
    double timeSinceLastEnemySpawn = 0;                                         //Timer variable for enemy spawns
    boolean isRussian = false;                                                  //Checks if easter egg has been activated
    double timeSinceRussianActivation = 0;                                      //Sees how long since easter egg has been activated
    while( true ) {                                                             //Indefinite game loop, breaking ends the level
      //Precursor steps
      resources.addTime( deltaTime );                                           //Stores amount of time game is being played
      timeSinceLastEnemySpawn += deltaTime;                                     //Updates spawn times
      if( timeSinceLastEnemySpawn > 1.2 ) {                                     //Spawn new enemy if one wasn't spawned recently
        for( int i = 0; i < resources.entities.length; i++ ) {                  //Finds next null place and puts an enemy there
          if( resources.entities[ i ] == null ) {
            resources.entities[ i ] = EnemySpawner.getEnemy();                  //EnemySpawner selects and initializes an enemy
            timeSinceLastEnemySpawn = 0;                                        //Resets counter
            break;  //Stop from making new enemies
          }
        }
      }

      int latestTexture = 0;                                                    //Next null position in graphics buffer
      //Draw background textures
      texts[ latestTexture ] = new DisplayObject( "background",                 //Draws background
        new int[]{ 0, 0 }, new int[]{ 1280, 720 } );
      latestTexture++;                                                          //Updates position

      if( resources.masterController.getR() && isRussian == false) {            //Activates easter egg
        resources.sound.stop();                                                 //Stops regular music
        resources.sound.loop( "russia" );                                       //Plays the russian music
        isRussian = true;                                                       //Activates russian mode
      }
      if( isRussian == true && timeSinceRussianActivation < 3 ) {               //Draws splashtext
          texts[ texts.length - 1 ] = new DisplayObject( "winter",              //Sets last value of graphics buffer to splashtext
            new int[]{ 0, 0 }, new int[]{ 1280, 720 } );                        //Graphics buffer is slightly oversized to accomodate
          timeSinceRussianActivation += deltaTime;                              //Updates timer
      }
      //Delete things
      for( int i = 1; i < resources.entities.length; i ++ ) {                   //All entities, ignoring player at index 0
        try {                                                                   //Attempts to delete value
          resources.projectiles[ ( (enemy)resources.entities[ i ] ).deleteIndex() ] = null;
        } catch( Exception n ) {}
        try {                                                                   //Deletes possible value
          if( ( (enemy)resources.entities[ i ] ).deleteCheck() ) {
            resources.entities[ i ] = null;
          }
        } catch( NullPointerException n ) {
          continue;
        }
      }

      //Tick everything
      for( int i = 0; i < resources.entities.length; i ++ ) { //Entities        //Tick entities
        if( resources.entities[ i ] != null ) {
          texts[ latestTexture] = resources.entities[ i ].tick( deltaTime );
          latestTexture++;
        }
      }
      for( int i = 0; i < resources.projectiles.length; i ++ ) { //projectiles  //Tick projectiles
        if( resources.projectiles[ i ] != null ) {
          texts[ latestTexture] = resources.projectiles[ i ].tick( deltaTime );
          if( resources.projectiles[ i ].deleteCheck() ) {
            resources.projectiles[ i ] = null;
          }
          latestTexture++;
        }
      }
      //Check for health conditions
      if( resources.getHealth() == 3 ) {                                        //Display 3 hearts when health is 3
        texts[ texts.length - 2 ] = new DisplayObject( "heart", new int[]{ 640 - 16 - 32, 0 }, new int[]{ 32, 32 } );
        texts[ texts.length - 3 ] = new DisplayObject( "heart", new int[]{ 640 - 16, 0 }, new int[]{ 32, 32 } );
        texts[ texts.length - 4 ] = new DisplayObject( "heart", new int[]{ 640 - 16 + 32, 0 }, new int[]{ 32, 32 } );
      } else if( resources.getHealth() == 2 ) {                                 //Display 2 hearts when health is 2
        texts[ texts.length - 2 ] = new DisplayObject( "heart", new int[]{ 640 - 16 - 32, 0 }, new int[]{ 32, 32 } );
        texts[ texts.length - 3 ] = new DisplayObject( "heart", new int[]{ 640 - 16, 0 }, new int[]{ 32, 32 } );
      } else if( resources.getHealth() == 1 ) {                                 //Displays 1 heart when heath is 1
        texts[ texts.length - 2 ] = new DisplayObject( "heart", new int[]{ 640 - 16 - 32, 0 }, new int[]{ 32, 32 } );
      } else if( resources.getHealth() <= 0 ) {                                 //Ends the while( true ) when health is 0
        break;
      }
      if( isRussian ) {                                                         //Fills the remaining graphics buffer with snowflakes
        while( latestTexture < MAX_TEXTURES - 10 ) {                            //Simulates a heavy snowstorm
          texts[ latestTexture ] = new DisplayObject( "snowflake", new int[]{ (int)( Math.random() * 1280 ), (int)( Math.random() * 720 ) }, new int[]{ 16, 16 } );
          latestTexture++;
        }
      }
      resources.addScore( deltaTime * 5 );                                      //Adds some score each frame
      //Draw
      disp.draw( texts );                                                       //Sends everything to display to be drawn

      //Reset
      texts = new DisplayObject[ MAX_TEXTURES ];                                //Clear graphics buffer
      //DeltaTime Calculations
      while( System.nanoTime() - startTime < 16700000 ) {                       //Make sure the frame time was slow enough
        try{ Thread.sleep( 1 ); } catch( Exception e ) {}                       //Waits until the frame is slower than 1/60 of a second
      }
      deltaTime = (double)( System.nanoTime() - startTime ) / 1000000000d;      //Save the amount of seconds to calculate the frame
      startTime = System.nanoTime();                                            //Resaves the start time
    }
  }
}
