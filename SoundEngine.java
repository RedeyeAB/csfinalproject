import javax.sound.sampled.*;                                                   //Imports
import java.io.*;

/**
 * A SoundEngine loads and manages sounds from a file
 */
public class SoundEngine {                                                      //Class header
  /**
   * Creates a SoundEngine object that can be used to easily load sounds from a file.
   */
  public SoundEngine() {}                                                       //Constructor
  /**
   * Is used to store the audio file when loaded into memory
   */
  Clip clip;                                                                    //Clip reference
  /**
   * Opens a sound file from assets/audio once.
   * @param The name of the selection
   */
  public void play( String selection ) {                                        //Load & Play sound
    try {
      AudioInputStream ais = AudioSystem.getAudioInputStream( new File( "assets/audio/" + selection + ".wav" ).getAbsoluteFile() );
      clip = AudioSystem.getClip();
      clip.open( ais );                                                         //Open from file and play
      clip.start();
    } catch( Exception e ) {}
  }
  /**
   * Opens a sound file from assets/audio once.
   * @param The name of the selection
   */
  public void loop( String selection ) {                                        //Uses loop forever
    try {
      AudioInputStream ais = AudioSystem.getAudioInputStream( new File( "assets/audio/" + selection + ".wav" ).getAbsoluteFile() );
      clip = AudioSystem.getClip();
      clip.open( ais );
      clip.loop( Clip.LOOP_CONTINUOUSLY );
    } catch( Exception e ) {}
  }
  /**
   * Stops any actively playing audio clip in memory (Clip).
   */
  public void stop() {                                                          //Stop the clip from playing again
      try {
          clip.stop();
      } catch( Exception e ) {}
  }
}
