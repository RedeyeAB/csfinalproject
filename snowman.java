/**
 * Defines the properties of a snowman enemy.
 */
public class snowman extends enemy implements entity {                          //Snowman class header
  /**
   * Defines the x-position of the enemy.
   */
  float xPos;                                                                   //x position
  /**
   * Defines the y-position of the enemy.
   */
  float yPos;                                                                   //y position
  /**
   * Defines the strength of the enemy.
   */
  double timer = 0.0;                                                           //time since throw

  /**
   * Creates a snowman that can be stored in memory.
   * @param x & y spawn position
   */
  public snowman( float xSpawn, float ySpawn ) {                                //Constructor
    xPos = xSpawn;
    yPos = ySpawn;
    level = (int)resources.getTime();
  }

  /**
   * Defines the default texture of a snowman.
   */
  String texture = "snowman_front";                                             //Default texture
  /**
   * Stores the amount of time since the last time the enemy was able to hurt the player.
   */
  double timeSincePunch = 0;                                                    //Stores time since punch
  /**
   * Stores whether or not this snowman should be deleted next cycle.
   */
  boolean delete = false;                                                       //Delete flag
  /**
   * Stores the index of the projectile that last hit the enemy.
   */
  int projectileIndex = -1;                                                     //Projectil to delete
  /**
   * Updates the properties of the enemy for the next frame.
   * @param The last time between frames
   */
  public DisplayObject tick( double dt ) {                                      //Updates position

    //throwing snowballs
    timer += dt;
    if( timer > 2.0 ) {                                                         //Checks time since throw
      timer = 0.0;
      double px = ( (player)resources.entities[ 0 ] ).xPos * 16;                //Player pixel coords
      double py = ( (player)resources.entities[ 0 ] ).yPos * 16;
      double xDist = xPos * 16 - px + 8;                                        //Hitbox
      double yDist = yPos * 16 - py + 8;
      double pyDist = Math.pow( yDist * yDist + xDist * xDist, .5 );            //Get distance
      for( int i = 0; i < resources.projectiles.length; i++ ) {                 //Put snowball in next null area
        if( resources.projectiles[ i ] == null ) {
          resources.projectiles[ i ] = new EnemySnowball(xPos * 16, yPos * 16, -1 * xDist/pyDist, -1 * yDist/pyDist);
          break;
        }
      }
    }

    //Check for projectiles
    for( int i = 0; i < resources.projectiles.length; i++ ) {                   //Hurt check
      if( resources.projectiles[ i ] != null ) {                                //Snowball isnt null
        if( ( (projectile)resources.projectiles[ i ] ).getType() == 'e' ){      //Meant to attack player
          int[] projectilePos = ( ( (projectile)                                //Check bound
            resources.projectiles[ i ] ).getBounds() );
          if( Math.abs( projectilePos[ 0 ] - xPos * 16 ) < 16 && Math.abs( projectilePos[ 1 ] - yPos * 16 ) < 16 ) {
            delete = true;                                                      //If close delete
            resources.addScore( 10 );                                           //Update score
            projectileIndex = i;                                                //Delete projectile
          }
        }
      }
    }
    //Calculates next position
    timeSincePunch += dt;                                                       //Updates time
    double px = ( (player)resources.entities[ 0 ] ).xPos;                       //Coords
    double py = ( (player)resources.entities[ 0 ] ).yPos;
    double dx = 0;
    double dy = 0;
    boolean isClose = true;
    if( ( xPos - px ) > 1 ) {                                                   //Calculate movements
      xPos += dx = -5 * dt;
      isClose = false;
    }
    if( ( xPos - px ) < -1 ) {
      xPos += dx = 5 * dt;
      isClose = false;
    }
    if( ( yPos - py ) > 1 ) {
      yPos += dy = -5 * dt;
      isClose = false;
    }
    if( ( yPos - py ) < -1 ) {
      yPos += dy = 5 * dt;
      isClose = false;
    }
    if( timeSincePunch > 2 && isClose == true ) {                               //Check for hit
      resources.addHealth( -1 );
      timeSincePunch = 0;
    }

    if( dy < 0 ) {                                                              //Textures based on movement
      texture = "snowman_back";
    }
    if( dy > 0 ) {
      texture = "snowman_front";
    }
    if( dx < 0 && Math.abs( dy / dt ) < 5d ) {
      texture = "snowman_left";
    }
    if( dx > 0 && Math.abs( dy / dt ) < 5d ) {
      texture = "snowman_right";
    }
    String dirtexture;
    return new DisplayObject( texture, new int[]{ (int)( 16 * xPos ), (int)( yPos * 16 ) }, new int[]{ 32, 32 } );
  }
  /**
   * Returns whether the snowman should be deleted or not next cycle.
   * @return Whether to delete this or not
   */
  public boolean deleteCheck() {                                                //Delete getter
    return delete;
  }
  /**
   * Returns the index of the snowball that last damaged this enemy.
   * @return the projectile index to delete
   */
  public int deleteIndex() {                                                    //Projectile gitter
    int temp = projectileIndex;
    projectileIndex = -1;
    return temp;
  }
}
