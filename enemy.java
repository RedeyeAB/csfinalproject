/**
 * Describes all enemies - anything that can attack the player.
 */
public abstract class enemy implements entity {                                 //Defines all enemies
  /**
   * Defines the x-position of the enemy.
   */
  float xPos;                                                                   //Last calculated x position
  /**
   * Defines the y-position of the enemy.
   */
  float yPos;                                                                   //Last calculated y position
  /**
   * Defines the strength of the enemy.
   */
  int level;                                                                    //Spawn level of enemy

  /**
   * Defines the tick function all enemies must have.
   * @param The last time between frames
   * @return The updated graphics data
   */
  public DisplayObject tick() {                                                 //Calculate next movements
    return new DisplayObject( "snowball", new int[]{ -100, -100 },              //Return generic texture for abstract
      new int[]{ 0, 0 } );
  }
  /**
   * Stores whether or not the enemy should be deleted from memory next computational cycle.
   */
  boolean delete = false;                                                       //Should delete?
  /**
   * A getter for delete.
   * @return Whether to delete this or not
   */
  public boolean deleteCheck() {                                                //delete getter
    return delete;
  }
  /**
   * Stores the index of the projectile that last hit the enemy.
   */
  int projectileIndex = 0;                                                      //Projectile to delete
  /**
   * Returns the index of the projectile that last hit the enemy.
   * @return The index of the projectile to delete
   */
  public int deleteIndex() {                                                    //projectileIndex getter
    return projectileIndex;
  }
}
