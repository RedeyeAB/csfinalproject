import java.awt.event.KeyAdapter;                                               //Imports
import java.awt.event.KeyEvent;

/**
 * Simplifies the process of checking for keyboard input.
 */
public class KeyChecker extends KeyAdapter {                                    //Class definition
  /**
   * Checks if specific keys are pressed, interacts with the controller in resources to output keys.
   * @param KeyEvent of last press
   */
  public void keyPressed( KeyEvent event ) {                                    //Runs on keypress
    int e = event.getKeyCode();                                                 //Gets id of key
    if( e == 87 ) {        //W                                                  //Sets controller
      resources.masterController.setW( true );
    } else if( e == 65 ) { //A
      resources.masterController.setA( true );
    } else if( e == 83 ) { //S
      resources.masterController.setS( true );
    } else if( e == 68 ) { //D
      resources.masterController.setD( true );
    } else if( e == 82 ) { //R
      resources.masterController.setR( true );
    } else if( e == 27 ) { //Esc                                                //Quick exit games
      System.exit( 0 );
    }
  }
  /**
   * Checks if specific keys are released, interacts with the controller in resources to output keys.
   * @param KeyEvent of last release
   */
  public void keyReleased( KeyEvent event ) {                                   //Runs on keyrelease
    int e = event.getKeyCode();
    if( e == 87 ) {        //W
      resources.masterController.setW( false );
    } else if( e == 65 ) { //A
      resources.masterController.setA( false );
    } else if( e == 83 ) { //S
      resources.masterController.setS( false );
    } else if( e == 68 ) { //D
      resources.masterController.setD( false );
    } else if( e == 82 ) {
      resources.masterController.setR( false );
    }
  }
}
