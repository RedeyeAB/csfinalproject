import java.awt.event.KeyAdapter;                                               //Imports everything
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.MouseInfo;
import java.util.concurrent.TimeUnit;
import javax.swing.*;
import java.awt.*;

/**
 * Simplifies user input into the program,
 */
public class controller {                                                       //Middleman object that listeners can use to update
  /**
   * Stores the last x position of the mouse.
   */
  private double mouseX = 0;                                                    //Stores the last known mouse click's x position
  /**
   * Stores the last y position of the mouse.
   */
  private double mouseY = 0;                                                    //Stores the last known mouse click's y position
  /**
   * Stores whether or not w is pressed
   */
  private boolean wIsPressed = false;                                           //Stores whether or not w is pressed
  /**
   * Stores whether or not a is pressed
   */
  private boolean aIsPressed = false;                                           //Stores whether or not a is pressed
  /**
   * Stores whether or not s is pressed
   */
  private boolean sIsPressed = false;                                           //Stores whether or not s is pressed
  /**
   * Stores whether or not d is pressed
   */
  private boolean dIsPressed = false;                                           //Stores whether or not d is pressed
  /**
   * Stores whether or not r is pressed
   */
  private boolean rIsPressed = false;                                           //Stores whether or not r is pressed
  /**
   * Stores whether or not the mouse is pressed
   */
  private boolean mouseIsPressed = false;                                       //Stores whether or not mouse is pressed

  /**
   * Getter for rIsPressed
   * @return if r is pressed
   */
  public boolean getR() {                                                       //r getter
      return rIsPressed;
  }

  /**
   * Getter for wIsPressed.
   * @return if w is pressed
   */
  public boolean getW() {                                                       //w getter
    return wIsPressed;
  }

  /**
   * Getter for aIsPressed.
   * @return if a is pressed
   */
  public boolean getA() {                                                       //a getter
    return aIsPressed;
  }

  /**
   * Getter for sIsPressed.
   * @return if s is pressed
   */
  public boolean getS() {                                                       //s getter
    return sIsPressed;
  }

  /**
   * Getter for dIsPressed.
   * @return if d is pressed
   */
  public boolean getD() {                                                       //d getter
    return dIsPressed;
  }

  /**
   * Setter for wIsPressed.
   * @param w is pressed
   */
  public void setW(Boolean p) {                                                 //w setter
    wIsPressed = p;
  }

  /**
   * Setter for aIsPressed.
   * @param a is pressed
   */
  public void setA(Boolean p) {                                                 //a setter
    aIsPressed = p;
  }

  /**
   * Setter for sIsPressed.
   * @param s is pressed
   */
  public void setS(Boolean p) {                                                 //s setter
    sIsPressed = p;
  }

  /**
   * Setter for dIsPressed.
   * @param d is pressed
   */
  public void setD(Boolean p) {                                                 //d setter
    dIsPressed = p;
  }

  /**
   * Setter for rIsPressed.
   * @param r is pressed
   */
  public void setR( Boolean p ) {                                               //r setter
      rIsPressed = p;
  }

  /**
   * Getter for mouseIsPressed.
   * @param mouse is pressed
   */
  public boolean getMouse() {                                                   //mouse getter
    return mouseIsPressed;
  }

  /**
   * Getter for the mouse x position.
   * @return Mouse X position
   */
  public double getMouseX() {                                                   //mouse x getter
    return mouseX;
  }

  /**
   * Getter for the mouse y position.
   * @return Mouse y position
   */
  public double getMouseY() {                                                   //mouse y getter
    return mouseY;
  }
}
