import javax.swing.*;                                                           //Imports
import java.awt.*;
import javax.imageio.ImageIO;
import java.awt.image.*;

/**
 * Mediate the transfer of graphics information to the JFrame reference in resources.
 */
public class display {                                                          //Mediates transfer of data for updating a frame
  /**
   * Creates a display object that can be repeatitly addressed.
   */
  public display() {}                                                           //Constructor
  /**
   * Sets up the next frame then draws it.
   * @param Array of DisplayObjects to draw
   */
  public void draw( DisplayObject[] items ) {                                   //Updates the screen
    resources.frame.setContentPane( new gamepanel( items ) );                   //Creates new panel for the frame with the graphics data
    resources.frame.revalidate();                                               //Resets the display
    resources.frame.repaint();                                                  //Redraws
  }
}
