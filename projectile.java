/**
 * Defines the basic properties of all projectiles.
 */
public abstract class projectile implements entity {                            //Defines all projectiles, class header
  /**
   * Stores whether or not to delete this projectile next cycle.
   */
  boolean delete = false;                                                       //Delete flag
  /**
   * Getter for whether or not to delete this projectile.
   * @return Whether to delete this or not
   */
  public boolean deleteCheck() {                                                //Delete getter
    return delete;
  }
  /**
   * Returns a value that represents what type of entity this hurts.
   * @return The type of thing this hurts
   */
  abstract public char getType();                                               //Returns hurt type
  /**
   * Gets the hitbox of the projectile.
   * @return The hitbox of this projectile
   */
  abstract public int[] getBounds();                                            //Returns hitbox
}
