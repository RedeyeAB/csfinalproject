import java.awt.*;                                                              //Imports
import javax.imageio.ImageIO;
/**
 * Acts as an intermediate to describe the information required to display something in a specific manner.
 */
public class DisplayObject {                                                    //Describes a texture and its position when being drawn
  /**
   * Stores the name of the texture as will be obtained from an imageLoader.
   */
  private String name;                                                          //Stores the name of the texture to use
  /**
   * Stores the pixel coordinate of an object to be drawn.
   */
  private int[] coords;                                                         //Stores the position to draw the texture
  /**
   * Displays the scale of an object to be drawn.
   */
  private int[] dispSize;                                                       //Stores the size to draw the texture
  /**
   * Stores the values.
   * @param Name of the texture, its position, and how large to draw
   */
  public DisplayObject( String texturename, int[] coordsplace, int[] size ) {   //Constructor
    name = texturename;
    coords = coordsplace;
    dispSize = size;
  }
  /**
   * A getter for the stored name of a texture.
   * @return The name of the texture to load
   */
  public String getTextureName() {                                              //Name getter
    return name;
  }
  /**
   * A getter for the stored coordinates.
   * @return The coordinate to draw at
   */
  public int[] getCoords() {                                                    //Coords getter
    return coords;
  }
  /**
   * A getter for the stored size.
   * @return the size to draw at
   */
  public int[] getSize() {                                                      //Size getter
    return dispSize;
  }
}
