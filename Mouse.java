import java.awt.event.MouseEvent;                                               //imports
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import javax.swing.SwingUtilities;

/**
 * Simplifies the process of interacting with the mouse.
 */
public class Mouse implements MouseListener {                                   //mouse input
  /**
   * Stores if the player has a snowball or not.
   */
  public boolean armed = true;                                                  //Checks for snowball
  /**
   * Does actions when the mouse is released.
   * @param MouseEvent of click
   */
  public void mouseReleased(MouseEvent me) { }                                  //Release
  /**
   * Does actions when the mouse is used.
   * @param MouseEvent of click
   */
  @Override
  public void mouseEntered(MouseEvent me) { }                                   //Mouse used
  /**
   * Does actions when the mouse isn't used.
   * @param MouseEvent of click
   */
  public void mouseExited(MouseEvent me) { }                                    //Mouse goes to other program
  /**
   * Does actions when the mouse is clicked.
   * @param MouseEvent of click
   */
  public void mouseClicked(MouseEvent me) { }                                   //click
  /**
   * Does actions when the mouse is pressed.
   * @param MouseEvent of click
   */
  public void mousePressed(MouseEvent me) {                                     //Shoot
    if( SwingUtilities.isLeftMouseButton(me) && armed == true ) {               //Check for shoot conditions
      double px = ( (player)resources.entities[ 0 ] ).xPos * 16;                //Get player x position
      double py = ( (player)resources.entities[ 0 ] ).yPos * 16;                //Get player y position
      double xDist = px - me.getX() + 8;                                        //Get x hitbox
      double yDist = py - me.getY() + 8;                                        //Get y hitbox
      double pyDist = Math.pow( yDist * yDist + xDist * xDist, .5 );                //Get distance
      for( int i = 0; i < resources.projectiles.length; i++ ) {                 //Add a new projectile
        if( resources.projectiles[ i ] == null ) {
          resources.projectiles[ i ] = new PlayerSnowball( px, py, -1 * xDist/pyDist, -1 * yDist / pyDist );
          break;
        }
      }
      armed = false;                                                            //Removes ball
      resources.hasBall = false;                                                //Removes ball
    } else if( SwingUtilities.isRightMouseButton(me) ) {                        //Addsball
      //if( Math.pow( Math.pow( me.getX() - ( (player)resources.entities[ 0 ] ).xPos * 16, 2 ) + Math.pow( me.getY() - ( (player)resources.entities[ 0 ] ).yPos * 16, 2 ), .5 ) < 128 ) {
      //  armed = true;                                                         //Proximity click, abusurdly difficult to play
      //}
      armed = true;                                                             //Pick ball
      resources.hasBall = true;                                                 //Pick ball
    }
  }
}
