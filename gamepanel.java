import javax.swing.*;                                                           //Imports
import java.awt.*;
import javax.imageio.ImageIO;
import java.awt.image.*;

/**
 * Displays the graphics for the game onto the screen.
 */
public class gamepanel extends JPanel {                                         //Defines a basic menu
  /**
   * Stores the properties of all images on screen.
   */
  private DisplayObject[] toDisp;                                               //Graphics buffer declaration
  /**
   * Make a new frame with properties as given.
   * @param DisplayObject[]
   */
  public gamepanel( DisplayObject[] items ) {                                   //Constructor
    toDisp = items;
  }

  /**
   * Draw textures on the JFrame in resources for the next frame.
   * @param Graphics of display
   */
  public void paintComponent( Graphics g ) {                                    //Places graphics on display
    for( int i = 0; i < toDisp.length; i++ ) {                                  //Loops through all graphics in toDisp
      if( toDisp[ i ] == null ) {   //Ignore null
        continue;
      }

      g.drawImage( (BufferedImage)resources.textures.obtain(                    //Obtains the bufferedimage from memory then draws it
        toDisp[ i ].getTextureName() ), toDisp[ i ].getCoords()[ 0 ],
        toDisp[ i ].getCoords()[ 1 ], toDisp[ i ].getSize()[ 0 ],
        toDisp[ i ].getSize()[ 1 ], null );
      g.setFont (new Font("Serif", Font.BOLD, 25));                             //Set font
      g.drawString("Score: " + Integer.toString(resources.getScore()), 10, 20); //Draw score
    }
    resources.frame.setFocusable(true);                                         //Gets the mouse to capture
    resources.frame.requestFocusInWindow();                                     //required for unix/linux based systems
  }
}
