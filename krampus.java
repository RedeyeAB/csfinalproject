/**
 * Defines the properties of a krampus enemy.
 */
public class krampus extends enemy implements entity {                          //Class header
  /**
   * Defines the x-position of the enemy.
   */
  float xPos;                                                                   //Last x position
  /**
   * Defines the y-position of the enemy.
   */
  float yPos;                                                                   //Last y position
  /**
   * Defines the strength of the enemy.
   */
  double timer = 0.0;                                                           //Get time since attack

  /**
   * Creates a krampus that can be stored in memory.
   * @param x & y spawn position
   */
  public krampus( float xSpawn, float ySpawn ) {                                //Creates krampus
    xPos = xSpawn;
    yPos = ySpawn;
    level = (int)resources.getTime();
  }
  /**
   * Defines the default texture.
   */
  String texture = "krampus_front";                                             //Default texture
  /**
   * Stores the amount of time since this enemy last hit the player.
   */
  double timeSincePunch = 0;                                                    //Time since attack
  /**
   * Stores whether or not this enemy should be deleted next cycle.
   */
  boolean delete = false;                                                       //Delete this flag
  /**
   * Stores the index of the projectile that
   */
  int projectileIndex = -1;                                                     //Last projectile to hit
  /**
   * Stores the current health of the krampus.
   */
  int health = 3;                                                               //Health
  /**
   * Calculates the next positions of the krampus.
   * @param The previous time between frames
   * @return The DisplayObject of the updates
   */
  public DisplayObject tick( double dt ) {                                      //Updates positions
    projectileIndex = -1;                                                       //Resets index
    //throw projectile

    timer += dt;                                                                //Updates existance clock
    if( timer > 1.0 ) {                                                         //Check for time since throw
      timer = 0.0;                                                              //Reset
      double px = ( (player)resources.entities[ 0 ] ).xPos * 16;                //Get player pixel x coords
      double py = ( (player)resources.entities[ 0 ] ).yPos * 16;                //Get player pixel y coords
      double xDist = xPos * 16 - px + 8;                                        //Stores x distance from player
      double yDist = yPos * 16 - py + 8;                                        //Stores y distance from player
      double pyDist = Math.pow( yDist*yDist + xDist*xDist, .5 );                //Gets master distance
      for( int i = 0; i < resources.projectiles.length; i++ ) {                 //Puts enemy snowball into next null slot in projectile buffer
        if( resources.projectiles[ i ] == null ) {
          resources.projectiles[ i ] = new EnemySnowball(xPos * 16,
            yPos * 16, -1 * xDist/pyDist, -1 * yDist/pyDist);

          break;
        }
      }
    }

    //Check for projectiles
    for( int i = 0; i < resources.projectiles.length; i++ ) {                   //Checks for offending projectiles
      if( resources.projectiles[ i ] != null ) {                                //Provided they exist
        if( ( (projectile)resources.projectiles[ i ] ).getType() == 'e' ){      //And attact the enemies
          int[] projectilePos = ( ( (projectile)                                //Get hitbox
          resources.projectiles[ i ] ).getBounds() );
          if( Math.abs( projectilePos[ 0 ] - xPos * 16 ) < 16 &&                //Check distance
            Math.abs( projectilePos[ 1 ] - yPos * 16 ) < 16 ) {

            health--;
            projectileIndex = i;
          }
        }
      }
    }
    if( health <= 0 ) {                                                         //Death check
      delete = true;                                                            //Set flag
      resources.addScore( 30 );                                                 //Bump score
    }
    //Calculates next position
    timeSincePunch += dt;                                                       //Updates timer
    double px = ( (player)resources.entities[ 0 ] ).xPos;                       //Gets player x coordinate
    double py = ( (player)resources.entities[ 0 ] ).yPos;                       //Gets player y coordinate
    double dx = 0;                                                              //Change in x
    double dy = 0;                                                              //Change in y
    boolean isClose = true;                                                     //Checks distance
    if( ( xPos - px ) > 1 ) {                                                   //Movement updates
      xPos += dx = -12 * dt;
      isClose = false;
    }
    if( ( xPos - px ) < -1 ) {
      xPos += dx = 12 * dt;
      isClose = false;
    }
    if( ( yPos - py ) > 1 ) {
      yPos += dy = -12 * dt;
      isClose = false;
    }
    if( ( yPos - py ) < -1 ) {
      yPos += dy = 12 * dt;
      isClose = false;
    }
    if( timeSincePunch > 2 && isClose == true ) {                               //Checks for valid punch
      resources.addHealth( -1 );                                                //Remove health
      timeSincePunch = 0;                                                       //Resets health
    }

    if( dy < 0 ) {                                                              //Determine motion of direction and show texture
      texture = "krampus_back";
    }
    if( dy > 0 ) {
      texture = "krampus_front";
    }
    if( dx < 0 && Math.abs( dy / dt ) < 5d ) {
      texture = "krampus_left";
    }
    if( dx > 0 && Math.abs( dy / dt ) < 5d ) {
      texture = "krampus_right";
    }
    String dirtexture;
    return new DisplayObject( texture, new int[]{ (int)( 16 * xPos ),           //Set up display
      (int)( yPos * 16 ) }, new int[]{ 32, 32 } );
  }
  /**
   * Is a getter as to delete this enemy or not.
   * @return Whether to delete or not
   */
  public boolean deleteCheck() {                                                //Getter for delete flag
    return delete;
  }
  /**
   * Returns the index of the last projectile to hit this krampus.
   * @return The projectile to delete
   */
  public int deleteIndex() {                                                    //Getter for delete index
    int temp = projectileIndex;
    projectileIndex = -1;
    return temp;
  }
}
