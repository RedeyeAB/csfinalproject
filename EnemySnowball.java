/**
 * Defines the snowball an enemy can throw
 */
public class EnemySnowball extends projectile implements entity {               //Define EnemySnowball
  private double x;                                                             //Last x position
  private double y;                                                             //Last y position
  private double xs;                                                            //Horizontal vector magnitude
  private double ys;                                                            //Vertical vector magnitude
  private double distanceTraveled = 0;                                          //Stores distance traveled
  /**
   * Makes a new snowball
   * @param x & y spawn position, y & x speed
   */
  public EnemySnowball( double xSpawn, double ySpawn, double xSpeed,            //Constructor
    double ySpeed ) {

    x = xSpawn;
    y = ySpawn;
    xs = xSpeed;
    ys = ySpeed;
  }
  /**
   * Flag as to whether or not to delete this
   */
  boolean delete = false;                                                       //Whether or not to delete
  /**
   * Updates the snowball position
   * @param The time between frames
   * @return The updated graphics data
   */
  public DisplayObject tick( double dt ) {                                      //Tick function derived from entity
    if( x < -16 || x > 1280 ) {                                                 //Checks if out of bounds
      delete = true;
    } else if( y < -16 || y > 720 ) {
      delete = true;
    }
    x += xs * dt * 40 * 16;                                                     //Calculates next x position
    y += ys * dt * 40 * 16;                                                     //Calculates next y position
    distanceTraveled += Math.pow( Math.pow( xs * dt * 40 * 16, 2 ) +            //Updates traveled distance
      Math.pow( ys * dt * 40 * 16, 2 ), .5 );
    if( distanceTraveled > 320 ) {                                              //Checks if the ball has traveled beyond range
      delete = true;                                                            //Delete next cycle if it has
    }
    return new DisplayObject( "enemysnowball", new int[]{ (int)x, (int)y }      //Send updated information
    , new int[]{ 16, 16 } );
  }
  /**
   * Whether or not to delete this
   * @return To delete this?
   */
  public boolean deleteCheck() {                                                //Delete getter
    return delete;
  }
  /**
   * Returns the hitbox of this
   * @return The coordinates
   */
  public int[] getBounds() {                                                    //Hitbox getter
    return new int[]{ (int)x, (int)y };
  }
  /**
   * Returns the type this ball attacks
   * @return The type this attacks
   */
  public char getType() {                                                       //Returns the type of thing ths attacks
    return 'p'; //Attacks player
  }
}
