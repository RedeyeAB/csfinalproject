/**
 * Defines the yeti enemy.
 */
public class yeti extends enemy implements entity {                             //Class header
  /**
   * Defines the x-position of the enemy.
   */
  float xPos;                                                                   //Position vars
  /**
   * Defines the y-position of the enemy.
   */
  float yPos;
  /**
   * Defines the strength of the enemy.
   */
  double timer = 0.0;

  /**
   * Creates a yeti that can be stored in memory.
   * @param x & y spawn position
   */
  public yeti( float xSpawn, float ySpawn ) {                                   //Constructor
    xPos = xSpawn;
    yPos = ySpawn;
    level = (int)resources.getTime();
  }
  /**
   * Defines the default texture of a yeti.
   */
  String texture = "Yeti";                                                      //Default texture
  /**
   * Stores the amount of time since the yeti last hit the player.
   */
  double timeSincePunch = 0;                                                    //Punch time
  /**
   * Stores whether or not this enemy should be deleted next cycle.
   */
  boolean delete = false;                                                       //Delete flag
  /**
   * Stores the index of the last snowball that hit the yeti.
   */
  int projectileIndex = -1;                                                     //Projectile to delete
  /**
   * Stores the health of this yeti.
   */
  int health = 2;                                                               //Health
  /**
   * Runs the calculations for the next frame of this yeti.
   * @param The last time between frames
   */
  public DisplayObject tick( double dt ) {                                      //Update positions
    projectileIndex = -1;

    //throw projectile
    timer += dt;
    if (timer > 1.5) {                                                          //Check to see when to throw again
      timer = 0.0;
      double px = ( (player)resources.entities[ 0 ] ).xPos * 16;                //calculate trajectory and throw
      double py = ( (player)resources.entities[ 0 ] ).yPos * 16;
      double xDist = xPos * 16 - px + 8;
      double yDist = yPos * 16 - py + 8;
      double pyDist = Math.pow(yDist*yDist + xDist*xDist, .5);
      for( int i = 0; i < resources.projectiles.length; i++ ) {
        if( resources.projectiles[ i ] == null ) {
          resources.projectiles[ i ] = new EnemySnowball(xPos * 16, yPos * 16, -1 * xDist/pyDist, -1 * yDist/pyDist);
          System.out.println("Snowman throws snowball");

          break;
        }
      }
    }


    //Check for projectiles
    for( int i = 0; i < resources.projectiles.length; i++ ) {                   //Check buffer for offending balls
      if( resources.projectiles[ i ] != null ) {                                //Balls exist
        if( ( (projectile)resources.projectiles[ i ] ).getType() == 'e' ){      //Can hurt enemies & are close
          int[] projectilePos = ( ( (projectile)resources.projectiles[ i ] ).getBounds() );
          if( Math.abs( projectilePos[ 0 ] - xPos * 16 ) < 16 && Math.abs( projectilePos[ 1 ] - yPos * 16 ) < 16 ) {
            health--;                                                           //Hurt
            projectileIndex = i;
          }
        }
      }
    }
    if( health <= 0 ) {                                                         //Delete flag
      delete = true;
      resources.addScore( 20 );                                                 //Score
    }
    //Calculates next position
    timeSincePunch += dt;                                                       //Update time
    double px = ( (player)resources.entities[ 0 ] ).xPos;
    double py = ( (player)resources.entities[ 0 ] ).yPos;
    double dx = 0;
    double dy = 0;
    boolean isClose = true;
    if( ( xPos - px ) > 1 ) {                                                   //Update velocity
      xPos += dx = -8 * dt;
      isClose = false;
    }
    if( ( xPos - px ) < -1 ) {
      xPos += dx = 8 * dt;
      isClose = false;
    }
    if( ( yPos - py ) > 1 ) {
      yPos += dy = -8 * dt;
      isClose = false;
    }
    if( ( yPos - py ) < -1 ) {
      yPos += dy = 8 * dt;
      isClose = false;
    }
    if( timeSincePunch > 2 && isClose == true ) {
      resources.addHealth( -1 );
      timeSincePunch = 0;
    }

    if( dy < 0 ) {                                                              //Textures with movements
      texture = "Yeti_back";
    }
    if( dy > 0 ) {
      texture = "Yeti";
    }
    if( dx < 0 && Math.abs( dy / dt ) < 5d ) {
      texture = "Yeti_left";
    }
    if( dx > 0 && Math.abs( dy / dt ) < 5d ) {
      texture = "Yeti_right";
    }
    String dirtexture;
    return new DisplayObject( texture, new int[]{ (int)( 16 * xPos ), (int)( yPos * 16 ) }, new int[]{ 32, 32 } );
  }
  /**
   * Is a getter for delete.
   * @return Whether to delete or not
   */
  public boolean deleteCheck() {                                                //Modify delete tag
    return delete;
  }
  /**
   * Is a getter for the projectile that last hit this enemy.
   * @return Index to delete
   */
  public int deleteIndex() {                                                    //Get index
    int temp = projectileIndex;
    projectileIndex = -1;
    return temp;
  }
}
