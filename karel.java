/**
 * Defines the properties of a karel enemy.
 */
public class karel extends enemy implements entity {                            //Karel class header
  /**
   * Defines the x-position of the enemy.
   */
  float xPos;                                                                   //Defines last x position
  /**
   * Defines the y-position of the enemy.
   */
  float yPos;                                                                   //Defines last y position
  /**
   * Defines the strength of the enemy.
   */
  int level;                                                                    //Defines strengh of karel
  double timer = 0.0;                                                           //Stores the time this has existed

  /**
   * Creates a karel that can be stored in memory
   * @param x & y spawn position
   */
  public karel( float xSpawn, float ySpawn ) {                                  //Makes a new karel
    xPos = xSpawn;
    yPos = ySpawn;
    level = (int)resources.getTime();
  }

  /**
   * Defines the default texture of a snowman.
   */
  String texture = "karel_front";                                               //Defines the texture to draw with by default
  /**
   * Stores the amount of time since the last time the enemy was able to hurt the player.
   */
  double timeSincePunch = 0;                                                    //Stores the time since the last throw
  /**
   * Stores whether or not this snowman should be deleted next cycle.
   */
  boolean delete = false;                                                       //Stores whether or not to delete this
  /**
   * Stores the index of the projectile that last hit the enemy.
   */
  int projectileIndex = -1;                                                     //Stores last projectile hit index
  /**
   * Stores the amount of health this karel has left.
   */
  int health = 4;                                                               //Stores health
  /**
   * Updates the properties of the enemy for the next frame.
   * @param last time between frames
   * @return DisplayObject to display
   */
  public DisplayObject tick( double dt ) {                                      //Updates position
    projectileIndex = -1;                                                       //Resets projectile position
    //karel.throwProjectile()

    timer += dt;                                                                //Updates time of existance
    if (timer > 0.5) {                                                          //Lobs snowball every .5 sec
      timer = 0.0;
      double px = ( (player)resources.entities[ 0 ] ).xPos * 16;                //Figures out the players x position in pixel coords
      double py = ( (player)resources.entities[ 0 ] ).yPos * 16;                //Figures out the players y position in pixel coords
      double xDist = xPos * 16 - px + 8;                                        //Finds relative x position
      double yDist = yPos * 16 - py + 8;                                        //Finds relative y position
      double pyDist = Math.pow(yDist*yDist + xDist*xDist, .5);                  //Finds how far away this enemy is from the player
      for( int i = 0; i < resources.projectiles.length; i++ ) {                 //Puts a projectile in the next null place in projectile buffer
        if( resources.projectiles[ i ] == null ) {
          resources.projectiles[ i ] = new EnemySnowball(xPos * 16, yPos * 16, -1 * xDist/pyDist, -1 * yDist/pyDist);
          break;
        }
      }
    }

    //Check for projectiles
    for( int i = 0; i < resources.projectiles.length; i++ ) {                   //Checks projectiles buffer for harmful things
      if( resources.projectiles[ i ] != null ) {                                //Makes sure the projectile exists there
        if( ( (projectile)resources.projectiles[ i ] ).getType() == 'e' ){      //Checks if it hurts enemies
          int[] projectilePos = ( ( (projectile)                                //Calculates healthbox
            resources.projectiles[ i ] ).getBounds() );

          if( Math.abs( projectilePos[ 0 ] - xPos * 16 ) < 16 && Math.abs(      //Checks for proximity
            projectilePos[ 1 ] - yPos * 16 ) < 16 ) {

            health--;                                                           //Subtract health if hit
            projectileIndex = i;                                                //Store index of snowball being destroyed
          }
        }
      }
    }
    if( health <= 0 ) {                                                         //Check for death
      delete = true;                                                            //Set delete flag true
      resources.addScore( 50 );                                                 //Update score
    }
    //Calculates next position
    timeSincePunch += dt;                                                       //Updates timer
    double px = ( (player)resources.entities[ 0 ] ).xPos;                       //Stores player x coord
    double py = ( (player)resources.entities[ 0 ] ).yPos;                       //Stores player y coord
    double dx = 0;                                                              //Stores change in x
    double dy = 0;                                                              //Stores change in y
    boolean isClose = true;                                                     //Player proximity flag
    if( ( xPos - px ) > 1 ) {                                                   //Finds x movement
      xPos += dx = -18 * dt;
      isClose = false;
    }
    if( ( xPos - px ) < -1 ) {                                                  //Finds x movement
      xPos += dx = 18 * dt;
      isClose = false;
    }
    if( ( yPos - py ) > 1 ) {                                                   //Finds y movement
      yPos += dy = -18 * dt;
      isClose = false;
    }
    if( ( yPos - py ) < -1 ) {                                                  //Finds y movement
      yPos += dy = 18 * dt;
      isClose = false;
    }
    if( timeSincePunch > 2 && isClose == true ) {                               //Check if the player can be punched (no movement, dx & dy = 0)
      resources.addHealth( -1 );
      timeSincePunch = 0;
    }

    if( dy < 0 ) {                                                              //Shows moving back
      texture = "karel_back";
    }
    if( dy > 0 ) {                                                              //Shows moving fowards
      texture = "karel_front";
    }
    if( dx < 0 && Math.abs( dy / dt ) < 5d ) {                                  //Shows moving left
      texture = "karel_left";
    }
    if( dx > 0 && Math.abs( dy / dt ) < 5d ) {                                  //Shows moving left
      texture = "karel_right";
    }
    String dirtexture;
    return new DisplayObject( texture, new int[]{ (int)( 16 * xPos ),           //Returns graphics data
      (int)( yPos * 16 ) }, new int[]{ 32, 32 } );
  }
  /**
   * Returns whether the karel should be deleted or not next cycle.
   * @return Whether to delete or not
   */
  public boolean deleteCheck() {                                                //Delete getter
    return delete;
  }
  /**
   * Returns the index of the snowball that last damaged this enemy.
   * @return The projectile index to delete.
   */
  public int deleteIndex() {                                                    //Gets index of projectile to delete
    int temp = projectileIndex;                                                 //Restores value in memory
    projectileIndex = -1;                                                       //Resets index
    return temp;
  }
}
