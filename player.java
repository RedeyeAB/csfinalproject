import java.awt.event.MouseEvent;                                               //import
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;

/**
 * Deals with the workings of the player and its controls.
 */
public class player implements entity {                                         //class header
  /**
   * Deals with the position of the player onscreen.
   */
  private float[] position = new float[ 2 ];                                    //Stores player position

  /**
   * Saves the amount of snowballs possessed by the player.
   */
  private int snowballCount = 1;  //Start with 1 snowball                       //Snowball count

  /**
   * Value to check whether or not the player is moving left.
   */
  private boolean goLeft = false;                                               //Is moving left?
  /**
   * Value to check whether or not the player is moving right.
   */
  private boolean goRight = false;                                              //Is moving right?
  /**
   * Value to check whether or not the player is moving up.
   */
  private boolean goUp = false;                                                 //Is moviing up?
  /**
   * Value to check whether or not the player is moving down.
   */
  private boolean goDown = false;                                               //Is moving down?
  /**
   * Stores the x-position in game coordinates.
   */
  public double xPos = 40;   //Game coordinates                                 //Define x start conditions
  /**
   * Stores the y-Position in game coordinates.
   */
  public double yPos = 23;                                                      //Define y start conditions

  /**
   * Assigns the player access to mouse movement
   */
  public player() {                                                             //Constructor
    resources.frame.addMouseListener( new Mouse() );                            //Add mouse listener
  }

  /**
   * Stores the amount of seconds the previous frame took to calculate
   */
  private double dt;                                                            //Store deltaTime
  /**
   * Stores whether or not the mouse has been pressed.
   */
  private boolean hasPressed = false;                                           //Store mousedown conditions
  /**
   * Stores whether or not a snowball has been created for a given click.
   */
  private boolean hasSpawned = false;                                           //Has the player spawned a snowball?
  /**
   * Runs the calculations for one frame of movement and control.
   */
  public DisplayObject tick( double deltaTime ) {                               //Calculates new positions
    //Set vars
    dt = deltaTime;                                                             //Resets deltaTime
    //Movement
    double dx = 0;                                                              //Change of x
    double dy = 0;                                                              //Change of y
    if( resources.masterController.getW() ) {                                   //Check for up movement
      yPos += dy = -20 * dt;   //Up
      if( yPos < 0 ) {  //Screen bounds, player coords
        yPos = 0;
      }
    }
    if( resources.masterController.getS() ) {                                   //Check for down movement
      yPos += dy = 20 * dt;   //Down
      if( yPos > 43 ) { //Screen bounds, player coords
        yPos = 43;
      }
    }
    if( resources.masterController.getA() ) {                                   //Check for left movement
      xPos += dx += -20 * dt;  //Left
      if( xPos < 0 ) {  //Screen bounds, player coords
        xPos = 0;
      }
    }
    if( resources.masterController.getD() ) {                                   //Check for right movement
      xPos += dx += 20 * dt;  //Right
      if( xPos > 78 ) {  //Screen bounds, player coords
        xPos = 78;
      }
    }
    String texture;
    if( resources.hasBall ) {                                                   //Draw textures
      texture = "player_front_snowball";                                        //One per direction
    } else {                                                                    //One with and without a snowball in hand
      texture = "player_front";
    }
    if( dy < 0 ) {
      if( !resources.hasBall ) {
        texture = "player_back";
      } else {
        texture = "player_back_snowball";
      }
    }
    if( dy > 0 ) {
      if( !resources.hasBall ) {
        texture = "player_front";
      } else {
        texture = "player_front_snowball";
      }
    }
    if( dx < 0 && Math.abs( dy / dt ) < 5d ) {
      if( !resources.hasBall ) {
        texture = "player_left";
      } else {
        texture = "player_left_snowball";
      }
    }
    if( dx > 0 && Math.abs( dy / dt ) < 5d ) {
      if( !resources.hasBall ) {
        texture = "player_right";
      } else {
        texture = "player_right_snowball";
      }
    }

    //check for projectile
    for( int i = 0; i < resources.projectiles.length; i++ ) {                   //Checks for hits
      if( resources.projectiles[ i ] != null ) {                                //Projectile exists
        if( ( (projectile)resources.projectiles[ i ] ).getType() == 'p' ){      //Can hurt the player
          int[] projectilePos = ( ( (projectile)                                //Gets hitbox, calculates distance
            resources.projectiles[ i ] ).getBounds() );
          if( Math.abs( projectilePos[ 0 ] - xPos * 16 ) < 16 && Math.abs( projectilePos[ 1 ] - yPos * 16 ) < 16 ) {
            resources.addHealth( -1 );                                          //Hurt
            resources.projectiles[ i ] = null;                                  //Delete projectile
          }
        }
      }
    }
    return new DisplayObject( texture, new int[]{ (int)( xPos * 16 ), (int)( yPos * 16 ) }, new int[]{ 32, 32 } );
  }
}
