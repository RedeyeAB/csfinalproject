import javax.swing.*;                                                           //Imports
import java.awt.*;
import javax.imageio.ImageIO;
import java.awt.image.*;
import java.awt.event.*;

/**
 * Displays the initial start screen on the JFrame in resources.
 */
public class menupanel extends JPanel {                                         //Updates menupanel
  /**
   * Checks as to whether or not to continue to the game.
   */
  private static boolean playPressed = false;                                   //Continue flag

  /**
   * Draw everything onto the JFrame in resources.
   * @param JFrame paint component
   */
  public void paintComponent( Graphics g ) {                                    //Draws menu
    JButton credits = new JButton("");                                          //Create button
    credits.setBounds(50, 500, 400, 150);                                       //Set size of button
    credits.setVisible(true);                                                   //Make button clickable
    credits.setOpaque(false);                                                   //Make button transparent
    credits.setContentAreaFilled(false);                                        //Remove content
    credits.setBorderPainted(false);                                            //Make invisible
    credits.addActionListener(new AbstractAction("credits") {                   //Add listener
      public void actionPerformed(ActionEvent e) {                              //Define listener
        JOptionPane.showMessageDialog(null, "Creators:" + "\n" + "Alex Black" + //Show info dialog
          "\n" + "Will Hancock" + "\n" + "Kareem Jaber" + "\n" + "\n" + "Music:" + "\n" + "Wii Fit Plus: Snowball Fight Music" + "\n" + "From Russia With Love", "Credits", JOptionPane.INFORMATION_MESSAGE);
        }
      }
    );
    resources.frame.add(credits);                                               //Add button to frame

    JButton play = new JButton("");                                             //Yet another invisible button, see above
    play.setBounds(525, 525, 300, 150);
    play.setVisible(true);
    play.setOpaque(false);
    play.setContentAreaFilled(false);
    play.setBorderPainted(false);
    play.addActionListener(new AbstractAction("play") {
      public void actionPerformed(ActionEvent e) {
        playPressed = true;
      }
    } );
    resources.frame.add(play);

    JButton exit = new JButton("");                                             //See above
    exit.setBounds(1000, 550, 250, 150);
    exit.setVisible(true);
    exit.setOpaque(false);
    exit.setContentAreaFilled(false);
    exit.setBorderPainted(false);
    exit.addActionListener(new AbstractAction("exit") {
      public void actionPerformed (ActionEvent e) {
        System.exit(0);
      }
    } );
    resources.frame.add(exit);


    g.drawImage( (BufferedImage)resources.textures.obtain( "menu" ),            //Draw background
      0, 0, null );
  }

  /**
   * Getter method as whether to continue to the game.
   * @return Whether or not the game has started.
   */
  public static boolean getPlay() {                                             //Getter for play flag
    return playPressed;
  }
}
