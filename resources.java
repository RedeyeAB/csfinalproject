import javax.swing.*;                                                           //Imports
import java.awt.*;
import javax.imageio.ImageIO;
import java.awt.image.*;

/**
 * Acts as a mediator of information needed in multiple classes.
 */
public class resources {                                                        //Is not initialized, just stores things
  /**
   * Is a reference to the name of the game.
   */
  public final static String GAME_NAME = "Snowball Showdown";                   //Game name
  /**
   * Contains all the texture names that are required for the proper function of the game.
   */
  public final static String[] TEXTURE_NAMES = new String[]{ "snowball", "snowman_back", "snowman_front", "snowman_right", "snowman_left", "karel_back", "karel_left", "karel_right", "karel_front",
    "player_front", "player_front_snowball", "player_back", "player_back_snowball", "player_right", "player_right_snowball", "player_left", "player_left_snowball", "background", "krampus_front",
    "krampus_back", "krampus_left", "krampus_right" , "Yeti", "Yeti_back", "Yeti_left", "Yeti_right", "winter", "snowflake", "menu", "play", "heart", "endpanel", "enemysnowball"};
  /**
   * Contains all the sound names that are required for the proper function of the game.
   */
  public final static String[] SOUND_NAMES = new String[]{ "music", "russia" }; //Sounds to loads

  /**
   * Instantiated in main, keeps a copy of all textures in memory to avoid excessive disk reads.
   */
  public static imageLoader textures;                                           //Reserves memory for imageloader
  /**
   * Instantiated in main, allows global ability to play audio.
   */
  public static SoundEngine sound;                                              //Reserves memory for soundloader
  /**
   * Instantiated in main, acts as the method for displaying graphics on-screen.
   */
  public static JFrame frame;                                                   //Reserves memory for jframe
  /**
   * Instantiated in main, acts as a master method for obtaining input from the user.
   */
  public static controller masterController;                                    //Reserves memory for controller

  /**
   * Instantiated in-game, acts as a global reference to all entities present at a given moment.
   */
  public static entity[] entities;                                              //Entity buffer
  /**
   * Instantiated in-game, acts as a global reference to all projectiles present at a given moment.
   */
  public static projectile[] projectiles;                                       //Projectile buffer
  /**
   * Sets the reference of a passed object to null. This works for arrays and specific indexes.
   * @param The object reference to nullify
   */
  public static void nullifyObject( Object o ) {                                //Sets something to null
    o = null;
  }

  //Counters
  /**
   * Keeps track of the score at a given moment.
   */
  private static double score = 0;                                              //Stores score
  /**
   * Keeps track of the player's health at a given moment.
   */
  private static int health = 3;                                                //Stores health
  /**
   * Keeps track of the time spent in-game by the player at a given moment.
   */
  private static double time = 0.0;                                             //Store in-game time
  /**
   * Keeps track of whether or not the player is holding a snowball.
   */
  public static boolean hasBall = true;                                         //Stores player ball status
  /**
   * Acts as a setter method to score.
   * @param Score to add
   */
  public static void addScore( double v ) {                                     //Score setter
    score = score + v;
  }
  /**
   * Acts as a getter method for score.
   * @return returns the current score
   */
  public static int getScore() {                                                //Score getter
    return (int)score;
  }
  /**
   * Acts as a setter method to health.
   * @param the health to modify
   */
  public static void addHealth( int h ) {                                       //Health setter
    health = health + h;
  }
  /**
   * Acts as a getter method for health.
   * @return The health left
   */
  public static int getHealth() {                                               //Health getter
    return health;
  }
  /**
   * Acts as setter method for time.
   * @param The amount of time to pass
   */
  public static void addTime( double t ) {                                      //Time setter
    time = time + t;
  }
  /**
   * Acts as a getter method for time.
   * @return The time since the game started
   */
  public static double getTime() {                                              //Time getter
    return time;
  }
}
