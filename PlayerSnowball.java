/**
 * Defines a snowball object.
 */
public class PlayerSnowball extends projectile implements entity {              //Class header
  /**
   * Stores the current x position of the snowball.
   */
  private double x;                                                             //Last x position
  /**
   * Stores the current y position of the snowball.
   */
  private double y;                                                             //Lasy y position
  /**
   * Stores the x-speed of the snowball.
   */
  private double xs;                                                            //Gets x speed
  /**
   * Stores the y-speed of the snowball.
   */
  private double ys;                                                            //Gets y speed
  /**
   * Stores the distance the snowball has traveled.
   */
  private double distanceTraveled = 0;                                          //Distance the ball has flown
  /**
   * Creates a snowball that can be stored in memory.
   * @param x & y spawn position, y & x speed
   */
  public PlayerSnowball( double xSpawn, double ySpawn, double xSpeed,           //Create ball
    double ySpeed ) {

    x = xSpawn;
    y = ySpawn;
    xs = xSpeed;
    ys = ySpeed;
  }
  /**
   * Stores whether or not the snowball should be deleted next cycle.
   */
  boolean delete = false;                                                       //Delete flag
  /**
   * Calculates the next positions of the snowball.
   * @param Time to render previous frame
   */
  public DisplayObject tick( double dt ) {                                      //Update ball position
    if( x < -16 || x > 1280 ) {                                                 //Screen bounds
      delete = true;
    } else if( y < -16 || y > 720 ) {
      delete = true;
    }
    x += xs * dt * 40 * 16;                                                     //Movement
    y += ys * dt * 40 * 16;
    distanceTraveled += Math.pow( Math.pow( xs * dt * 40 * 16, 2 ) +            //Calculates travel distance
      Math.pow( ys * dt * 40 * 16, 2 ), .5 );
    if( distanceTraveled > 320 ) {                                              //Delete if too far
      delete = true;
    }
    return new DisplayObject( "snowball", new int[]{ (int)x, (int)y },          //Delete value
    new int[]{ 16, 16 } );
  }
  /**
   * Getter for whether or not this snowball should be deleted.
   * @return Whether or not to delete this
   */
  public boolean deleteCheck() {                                                //Delete getter
    return delete;
  }
  /**
   * Getter for the hitbox of this snowball.
   * @return The hitbox of this ball
   */
  public int[] getBounds() {                                                    //Bounding box getter
    return new int[]{ (int)x, (int)y };
  }
  /**
   * Returns what type of thing this projectile attacks.
   * @return Type of ball 
   */
  public char getType() {                                                       //Attack type
    return 'e'; //Attacks enemy
  }
}
