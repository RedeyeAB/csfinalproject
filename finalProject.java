/*/// Alexander Black, Kareem Jaber, and William Hancock
   CS Final Project: SNOWBALL SHOWDOWN
*///--------------------------------------------------------------------
import javax.swing.*;                                                           //Import

/**
 * The class that starts the game and its components.
 */
public class finalProject {                                                     //Class header
    //High level methods
    /**
     * The main method. Starts everything.
     */
    public static void main( String [] args ) {                                 //main()
        //Complete resources initialization
        resources.textures = new imageLoader( resources.TEXTURE_NAMES );        //Creates new imageloader in resource's static variable textures.
        resources.sound = new SoundEngine();                                    //Creates new soundengine in resource's static variable  sound.
        createPanel();                                                          //Calls local method createPanel()
        display disp = new display();

        //Actual code
        resources.sound.loop( "music" );                                        //Starts music
        displayMenu();                                                          //Calls local method displayMenu()
        while (true) {                                                          //Stops menu from ending until set so
          try {
            Thread.sleep(100);                                                  //Checks every 100ms for an update
          } catch (Exception e) {}
          if( menupanel.getPlay() ) {
            break;
          }
        }

        displayGame();                                                          //Calls local method displayGame()
        engine eng = new engine();                                              //Creates engine

        displayEnd();                                                           //Displays the last screen. Doesn't work until the engine finishes.
    }

    //Helper methods
    /**
     * Shortcut for the commands required to start the menu.
     */
    private static void displayMenu() {                                         //Sets up a menupanel and displays it.
        resources.frame.setContentPane( new menupanel() );                      //resources.frame is a reference to a globally accessable jframe
        resources.frame.setVisible( true );
    }

    /**
     * Shortcut for the commands required to start the game.
     */
    private static void displayGame() {                                         //Sets up and displays an initial gamepanel that can be later reset
        resources.frame.setContentPane( new gamepanel( new DisplayObject[ 0 ] ) );
        resources.frame.setVisible( true );
    }
    /**
     * Shortcut for the commands required to end the game and show the score screen.
     */
    private static void displayEnd() {                                          //Sets up and displays the score screen
        resources.frame.setContentPane( new endpanel() );
        resources.frame.setVisible( true );
    }
    /**
     * Shortcut for the commands required to initialize the JFrame in resources.
     */
    private static void createPanel() {                                         //Sets up the initial jframe in resources.frame
        resources.frame = new JFrame( resources.GAME_NAME );                    //Creates jframe
        resources.frame.setSize( 1280, 720 );                                   //Make jframe 1280 by 720 px
        resources.frame.setLocation( 100, 100 );                                //Make the top corner at (100px,100px)
        resources.frame.setUndecorated(true);                                   //Makes a borderless window
        resources.frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );       //Kills the program on exit
        resources.masterController = new controller();                          //Sets up a getter object for the keyboard and mouse in resources.masterController

        resources.frame.addKeyListener( new KeyChecker() );                     //Sets up class to update resources.masterController
        //ContentPane & setVisible( true ) run by method that loads the level
    }
}
