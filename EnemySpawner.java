/**
 * Quick reference for generating new enemies.
 */
public class EnemySpawner {
  /**
   * Generates a new random enemy.
   */
  public static enemy getEnemy() {
    double rand = Math.random();
    if( rand < .45 ) {
      return new snowman( (int)( Math.random() * 78 ), (int)( Math.random() * 43 ) );
    } else if( rand < .75 ) {
      //return new yeti();
      return new yeti( (int)( Math.random() * 78 ), (int)( Math.random() * 43 ) );
    } else if( rand < .99 ) {
      //return new krampus();
      return new krampus( (int)( Math.random() * 78 ), (int)( Math.random() * 43 ) );
    } else {
      //return new karel();
      return new karel( (int)( Math.random() * 78 ), (int)( Math.random() * 43 ) );
    }
  }
}
