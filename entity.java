/**
 * Describes a generic enemy.
 */
public interface entity {                                                       //Describe a basic entitys
  /**
   * Describes the tick function all entities have.
   * @param The last time between frames
   * @return The graphics data of the update
   */
  public DisplayObject tick( double deltaTime );                                //Defines the tick function
}
