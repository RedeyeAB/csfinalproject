import java.io.*;                                                               //Imports
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;

/**
 * Simplifies and accelerates the obtaining of images into memory.
 */
public class imageLoader {                                                      //Class header
  /**
   * Stores loaded images.
   */
  BufferedImage[] image;                                                        //Stores images in memory
  /**
   * Stores the name of the images in order of being loaded
   */
  String[] names;                                                               //Stores corresponding texture
  /**
   * Attempts to load images into memory using names as inputted.
   * @param String[] of file to load
   */
  public imageLoader( String[] files ) {                                        //Starts storing process
    names = files.clone();    //Makes copy so any changes to files that may occur don't effect other methods
    image = new BufferedImage[ files.length ];                                  //Creates buffer
    for( int i = 0; i < files.length; i++ ) {                                   //Attempts to load files in order
      try{
        image[ i ] = ImageIO.read( new File( "assets/textures/" + names[ i ] + ".png" ) );
      } catch( IOException e ) {
         try{
        image[ i ] = ImageIO.read( new File( "assets/textures/" + names[ i ] + ".jpg" ) );} //I see what you did
        catch( IOException f ) {
        System.out.println( "Bad texture name: " + files[ i ] );
        System.exit( 0 );}
      }
    }
  }
  /**
   * Passes the memory location of a requested image
   * @param Name of the file
   * @return BufferedImage to display
   */
  public BufferedImage obtain( String name ) {                                  //Returns the memory location of a bufferedimage
    for( int i = 0; i < names.length; i++ ) {                                   //Searches for data value
      if( names[ i ] == name ) {
        return image[ i ];
      }
    }
    System.out.println( "Texture not found: " + name );
    System.exit( 0 );
    return new BufferedImage(3,3,3);  //Nonsense statement, after System.exit()
  }
}
